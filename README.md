## How to build and deploy

## Create a server on Medstack Control
1. Go to the "Configure" tab
2. Create a new server group or use an existing one
3. Click new server and choose a size, then hit save
4. Wait 10 to 12 minutes for the server to be provisioned.

## Create a registry on Gitlab
See [https://docs.gitlab.com/ee/user/project/container_registry.html](https://docs.gitlab.com/ee/user/project/container_registry.html) for how to create a registry after you've signed up for Gitlab.

## Authenticate with the Gitlab Docker registry.
1. Click on *Registries* under your server group on the Configure page.
2. Click the link that says to Authenticate.
3. Enter the URL of the registry (registry.gitlab.com)
4. Enter your username and password, and click *Add Registry*

## Build the docker image and push it
1. Build the image: `docker-compose -f docker-compose-prod.yml build`

2. Get the image id from the output of the first command, e.g. c015bfa87b5c

3. Come up with a tag for the docker image, e.g. `1.0` (use a versioning system
   so that tags refer to releases)

4. Run `docker tag $IMAGE_ID registry.gitlab.com/yourregistry/flask-demo:$YOURTAG`

5. Run `docker push registry.gitlab.com/yourregistry/flask-demo:$YOURTAG`

## Create a domain name (Optional)
* Optionally Create a domain name
  * We do not run a DNS service, so if you do not set a domain name then the
    certificate will be self-signed and you may see a warning when you go to
    the site.

## Create the services on Medstack Control
* Create a postgresql service via the *Managed Service* button.
* Create a load balancer via the *Managed Service* button.

* Create an app service and fill out these options (leave others blank)
	- Name: flask
	- Image: registry.gitlab.com/yourregistry/flask-demo:$YOURTAG
	- Hostname: The hostname you registered, or leave it blank if you did not
    register one
	- Load balancer Port: 80
	- Load balancer Hosts: your public IP address OR the domain name you registered
	- Add environment variables, `DB_HOST` set to `postgresql` and `DB_NAME` set to `testdb_prod`

