FROM jfloff/alpine-python:3.4-onbuild
MAINTAINER wes kerfoot "wjak56@gmail.com"
COPY . ./src
WORKDIR /src
RUN apk update && apk add libpq postgresql-libs postgresql-dev postgresql-client

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    python3.4-dev \
    musl-dev \
    postgresql-dev \
    postgresql-libs \
    libpq \
    && apk del --no-cache .build-deps

RUN pip install psycopg2
RUN pip install -r /src/requirements.txt
ENTRYPOINT ["python"]
CMD ["/src/app.py"]
EXPOSE 80
